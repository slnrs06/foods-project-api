package com.foods.foods.repositories;

import com.foods.foods.models.Country;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
}