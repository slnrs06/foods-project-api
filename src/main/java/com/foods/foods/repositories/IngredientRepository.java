package com.foods.foods.repositories;



import com.foods.foods.models.Ingredient;

import org.springframework.data.jpa.repository.JpaRepository;


public interface IngredientRepository extends JpaRepository<Ingredient, Long> {
    
}