package com.foods.foods.controllers;

import java.util.List;
import java.util.Optional;

import com.foods.foods.models.Food;
import com.foods.foods.repositories.FoodRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("foods")

public class FoodController {
    @Autowired
    private FoodRepository foodRepo;


    @GetMapping("/all")
    public ResponseEntity<List<Food>> getAllFoods() {
        return ResponseEntity.ok(foodRepo.findAll());
    }

    @PostMapping("create")
    public ResponseEntity<Food> createFood(@RequestBody Food food) {
      
        return ResponseEntity.ok(foodRepo.save(food));
    }


    @PutMapping("update")
    public ResponseEntity<Food> update(@RequestBody Food food) {


        Optional<Food> f = foodRepo.findById(food.getId());
        if (f.isPresent()) {
            food.setId(f.get().getId());

        foodRepo.save(food);
        return ResponseEntity.ok(foodRepo.save(food));
    }
    return null;
    }


    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<String> deleteFood(@PathVariable Long id) {
        Optional<Food> food = foodRepo.findById(id);
        if (food.isPresent()) {
            foodRepo.delete(food.get());
            return ResponseEntity.ok("Deleted");
        }
        return ResponseEntity.ok("Not Found");
    }


}