package com.foods.foods.controllers;

import java.util.List;
import java.util.Optional;

import com.foods.foods.models.Ingredient;
import com.foods.foods.repositories.IngredientRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("ingredients")
public class IngredientsController {
    @Autowired
    private IngredientRepository ingredientRepo;
    @GetMapping("/all")
    public ResponseEntity<List<Ingredient>> getAll() {
        return ResponseEntity.ok(ingredientRepo.findAll());
    }
    @PostMapping("create")
    public ResponseEntity<Ingredient> create(@RequestBody Ingredient ingredient) {
        return ResponseEntity.ok(ingredientRepo.save(ingredient));
    }
    @PutMapping("update/{id}")
    public ResponseEntity<Ingredient> update(@RequestBody Ingredient ingredient) {
        Optional<Ingredient> f = ingredientRepo.findById(ingredient.getId());
        if (f.isPresent()) {
            ingredient.setId(f.get().getId());
            ingredientRepo.save(ingredient);
            return ResponseEntity.ok(ingredient);
        }
        return null;
    }
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<String> deleteFruit(@PathVariable Long id) {
        Optional<Ingredient> ingredient = ingredientRepo.findById(id);
        if (ingredient.isPresent()) {
            ingredientRepo.delete(ingredient.get());
            return ResponseEntity.ok("Deleted");
        }
        return ResponseEntity.ok("Not Found");
    }






//     @PostMapping("create-ingredients-list")
//     public ResponseEntity<List<Ingredient>> createIngredientList(@RequestBody List<Ingredient> ingredients) {
//         ingredientRepo.saveAll(ingredients);
//         return ResponseEntity.ok(ingredients);
//     }
//     @GetMapping("get-list-by/{countryNm}/{foodNm}")
//     public ResponseEntity<List<Ingredient>> getIngredientByCountryAndFood(@PathVariable String countryNm,@PathVariable String foodNm) {
//         return ResponseEntity.ok(ingredientRepo.getIngredientsByCountryAndFood(countryNm, foodNm));
//     }
// }
}