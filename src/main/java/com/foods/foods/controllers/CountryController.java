package com.foods.foods.controllers;

import java.util.List;
import java.util.Optional;

import com.foods.foods.models.Country;
import com.foods.foods.repositories.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("country")
public class CountryController {
    @Autowired
    private CountryRepository countryRepo;
    @GetMapping("/all")
    public ResponseEntity<List<Country>> getAll() {
        return ResponseEntity.ok(countryRepo.findAll());
    }
    @PostMapping("create")
    public ResponseEntity<Country> create(@RequestBody Country country) {
        return ResponseEntity.ok(countryRepo.save(country));
    }
    @PutMapping("update")
    public ResponseEntity<Country> update(@RequestBody Country country) {
        Optional<Country> c = countryRepo.findById(country.getId());
        if (c.isPresent()) {
            country.setId(c.get().getId());
            countryRepo.save(country);
            return ResponseEntity.ok(country);
        }
        return null;
    }
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<Country> country = countryRepo.findById(id);
        if (country.isPresent()) {
            countryRepo.delete(country.get());
            return ResponseEntity.ok("Deleted");
        }
        return ResponseEntity.ok("Not Found");
    }
}
